﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BacteriaSpawnerController : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] bacteria;
    int randomSpawnPoint, randomBacteria;
    public static bool spawnAllowed;
    private int bacteriaID;

    // Start is called before the first frame update
    void Start()
    {
        spawnAllowed = true;
        InvokeRepeating ("SpawnABacteria", 0f, 1f);
        bacteriaID = 0;
    }

    public void SpawnABacteria(){
        if(spawnAllowed){
            randomSpawnPoint = Random.Range(0, spawnPoints.Length);
            randomBacteria = Random.Range(0, bacteria.Length);

            if(bacteria[randomBacteria] != null)
            {
                GameObject newBacteria;
                newBacteria = Instantiate(bacteria[randomBacteria], spawnPoints[randomSpawnPoint].position, Quaternion.identity);

                if (newBacteria != null)
                {
                    BacteriaController bc = newBacteria.GetComponent<BacteriaController>();
                    if (bc != null)
                    {
                        //enable moving only for spawned objects not original objects
                        //this is because if player eats all objects we cannot instantiate anymore
                        //originals shall stay outside scene to prevent this
                        bc.setId(bacteriaID++);
                        bc.EnableMove();
                    }
                }
            }

            //spawnAllowed = false;
        }
    }
}
