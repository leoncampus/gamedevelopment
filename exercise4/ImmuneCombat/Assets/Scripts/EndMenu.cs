﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndMenu : MonoBehaviour
{
    public GameObject panel;
    public GameObject winText;
    public GameObject looseText;

    void Start()
    {
        panel.gameObject.SetActive(false);
        winText.gameObject.SetActive(false);
        looseText.gameObject.SetActive(false);
    }

    public void ShowPanel(string text)
    {
        panel.gameObject.SetActive(true);
        if (text == "loose")
        {
            looseText.gameObject.SetActive(true);
        }
        else
        {
            winText.gameObject.SetActive(true);
        }
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
