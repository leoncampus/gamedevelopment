﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodCellController : MonoBehaviour
{

    public float speed = 1.5f;
    public float rotateSpeed = 5.0f;
    float lifetime = 30.0f; //destroy blood cell after 30 seconds
    private bool move = false;

    Vector3 newPosition;

    public void EnableMove()
    {
        move = true;
    }

    void Start()
    {
        PositionChange();
        Destroy(gameObject, lifetime);
    }

    void PositionChange()
    {
        newPosition = new Vector2(Random.Range(-5.0f, 5.0f), Random.Range(-5.0f, 5.0f));
    }

    void Update()
    {
        if (move == true)
        {
            if (Vector2.Distance(transform.position, newPosition) < 1)
                PositionChange();

            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * speed);

            LookAt2D(newPosition);
        }
            
    }

    void LookAt2D(Vector3 lookAtPosition)
    {
        float distanceX = lookAtPosition.x - transform.position.x;
        float distanceY = lookAtPosition.y - transform.position.y;
        float angle = Mathf.Atan2(distanceX, distanceY) * Mathf.Rad2Deg;

        Quaternion endRotation = Quaternion.AngleAxis(angle, Vector3.back);
        transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime * rotateSpeed);
    }
}
