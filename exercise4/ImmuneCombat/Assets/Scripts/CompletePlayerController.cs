﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class CompletePlayerController : MonoBehaviour {

	public float speed;				//variable to store the players movement speed.
	public Text countText;			
    public Animator animator;
    public Slider healthbar;

    private Vector3 scale;
	private Rigidbody2D rb2d;		
	private int count;				
    private float growSize;

	void Start()
	{
        scale = transform.localScale;
        rb2d = GetComponent<Rigidbody2D> ();

        growSize = 1f;
		count = 0;
		SetCountText ();
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);

		//supplying movement multiplied by speed to move our player.
		rb2d.AddForce (movement * speed);
	}

    //do an animation for given seconds
    IEnumerator TimeEatingAnimation(string boolName, int seconds)
    {
        yield return new WaitForSeconds(seconds);
        animator.SetBool(boolName, false);
    }

    void OnTriggerEnter2D(Collider2D other) 
	{
        if (other.gameObject.CompareTag ("Bacteria")) 
		{
            //play sound
            SoundManagerController.PlaySound("eatBacteria");
            //do isEating animation
            animator.SetBool("isEating", true);
            //timer to end isEating animation
            StartCoroutine(TimeEatingAnimation("isEating", 2));

            //kill object
            Destroy(other.gameObject);

            //update counter text
            count = count + 1;
            SetCountText ();
        }
        else if (other.gameObject.CompareTag("Bloodcell"))
        {
            //play sound
            SoundManagerController.PlaySound("eatBloodcell");
            //do isEatingBloodCell animation
            animator.SetBool("isEatingBloodCell", true);
            //timer to end isEating animation
            StartCoroutine(TimeEatingAnimation("isEatingBloodCell", 2));

            Destroy(other.gameObject);

            if (healthbar.value - 4 >= 0)
            {
                healthbar.value -= 4;
            }
            else
            {
                healthbar.value -= 4;
                //YOU LOOSE
                SoundManagerController.PlaySound("loose");
                Time.timeScale = 0;
                //show retry button panel 
                EndMenu endmenu = GetComponent<EndMenu>();
                endmenu.ShowPanel("loose");
            }

        }
    }

    void SetCountText()
	{
		countText.text = "Count: " + count.ToString ();

	    //win!
		if (count >= 10)
        {
            SoundManagerController.PlaySound("win");
            Time.timeScale = 0;
            //show retry button panel 
            EndMenu endmenu = GetComponent<EndMenu>();
            endmenu.ShowPanel("win");
        }

    }

    private void PauseGame()
    {
        Time.timeScale = 0;
    }
}
