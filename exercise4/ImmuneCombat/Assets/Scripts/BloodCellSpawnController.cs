﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodCellSpawnController : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] bloodCells;
    int randomSpawnPoint, randomBloodCell;

    public static bool spawnAllowed;

    void Start()
    {
        spawnAllowed = true;
        InvokeRepeating("SpawnABloodCell", 0f, 2f);
    }

    public void SpawnABloodCell()
    {
        if (spawnAllowed)
        {
            randomSpawnPoint = Random.Range(0, spawnPoints.Length);
            randomBloodCell = Random.Range(0, bloodCells.Length);

            if(bloodCells[randomBloodCell] != null)
            {
                GameObject newBloodcell;
                newBloodcell = Instantiate(bloodCells[randomBloodCell], spawnPoints[randomSpawnPoint].position, Quaternion.identity);

                if (newBloodcell != null)
                {
                    BloodCellController bc = newBloodcell.GetComponent<BloodCellController>();
                    if (bc != null)
                    {
                        //enable moving only for spawned objects not original objects
                        //this is because if player eats all objects we cannot instantiate anymore
                        //originals shall stay outside scene to prevent this
                        bc.EnableMove();
                    }
                }
            }
        }
    }
}
