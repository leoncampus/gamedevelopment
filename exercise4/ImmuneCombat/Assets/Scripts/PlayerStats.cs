﻿public static class PlayerStats
{
    private static int count;
    private static string endText;

    public static int Count
    {
        get
        {
            return count;
        }
        set
        {
            count = value;
        }
    }

    public static string EndText
    {
        get
        {
            return endText;
        }
        set
        {
            endText = value;
        }
    }
}

