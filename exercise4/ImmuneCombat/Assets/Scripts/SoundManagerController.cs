﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerController : MonoBehaviour
{
    public static AudioClip playerEating, playerHurting, playerWinning, playerLoosing, backgroundMusic;
    static AudioSource audiosrc;

    void Start()
    {
        playerEating = Resources.Load<AudioClip>("good");
        playerHurting = Resources.Load<AudioClip>("bad");
        playerWinning = Resources.Load<AudioClip>("win");
        playerLoosing = Resources.Load<AudioClip>("lose");

        audiosrc = GetComponent<AudioSource>();

    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "eatBacteria":
                audiosrc.PlayOneShot(playerEating);
                break;
            case "eatBloodcell":
                audiosrc.PlayOneShot(playerHurting);
                break;
            case "win":
                audiosrc.PlayOneShot(playerWinning);
                break;
            case "loose":
                audiosrc.PlayOneShot(playerLoosing);
                break;
            case "background":
                audiosrc.loop = true;
                audiosrc.clip = backgroundMusic;
                audiosrc.Play();
                break;
        }
    }
}
