﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BacteriaController : MonoBehaviour
{

    private float latestDirectionChangeTime;
    private readonly float directionChangeTime = 3f;
    private float characterVelocity = 2f;
    private Vector2 movementDirection;
    private Vector2 movementPerSecond;
    private bool move = false;
    private int id;//needed for debugging reasons

    private Vector2 sceneCenter;
    private Vector2 position;
    private bool enteredScene = false;    //spawnpoints are outside sceen - this is for checking if bacteria has entered scene once

    void Start()
    {
        sceneCenter = new Vector2(0.0f, 0.0f);
        position = gameObject.transform.position;

        latestDirectionChangeTime = 0f;
        calcuateNewMovementVector();
    }

    public void setId(int newid)
    {
        id = newid;
    }

    public int getId()
    {
        return id;
    }

    public void EnableMove()
    {
        move = true;
    }

    //wait for x seconds before allow changing direction from center to random
    IEnumerator WaitTimeBeforeAllowDirectionChange(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        enteredScene = true;
    }

    void calcuateNewMovementVector()
    {
        //create a random direction vector with the magnitude of 1, later multiply it with the velocity of the bacteria
        movementDirection = new Vector2(UnityEngine.Random.Range(-1.0f, 1.0f), UnityEngine.Random.Range(-1.0f, 1.0f)).normalized;
        movementPerSecond = movementDirection * characterVelocity;
    }

    void  OnBecameVisible()
    {
        StartCoroutine(WaitTimeBeforeAllowDirectionChange(2));
    }

    void OnBecameInvisible()
    {
        if (move == true && enteredScene == true)
        {
            Destroy(gameObject);
        } 
    }

    void Update()
    {
        if(move == true)
        {

            //if entered scene once move randomly
            if (enteredScene == true)
            {
                //if the changeTime was reached, calculate a new movement vector
                if (Time.time - latestDirectionChangeTime > directionChangeTime)
                {
                    latestDirectionChangeTime = Time.time;
                    calcuateNewMovementVector();
                }

                //move bacteria: 
                transform.position = new Vector2(transform.position.x + (movementPerSecond.x * Time.deltaTime),
                transform.position.y + (movementPerSecond.y * Time.deltaTime));
            }
            else //move into scene center direction
            {
                transform.position = Vector2.MoveTowards(transform.position, sceneCenter, (characterVelocity * Time.deltaTime));
            }
        }

    }
    
}
